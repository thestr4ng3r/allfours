package allfours;

import java.io.IOException;

import allfours.ui.GamePanel;
import allfours.ui.MessageBox;
import allfours.ui.ProgramWindow;

/**
 * 
 * Grundlegende Klasse für das Spiel-Programm
 *
 */
public class AllFoursProgram implements Runnable
{
	private static final long MIN_TICK_TIME = 40;
	
	public static void main(String[] args) { new AllFoursProgram(); }
	
	private Game game;
	private GameData data;
	private GamePanel game_panel;
	private ProgramWindow window;
	
	public AllFoursProgram()
	{
		window = new ProgramWindow(this);
		try
		{
			data = new GameData("data");
		}
		catch (IOException e)
		{
			System.out.println("Fail: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		game = new Game(this);
		window.add(game_panel = new GamePanel(data, game));
		game_panel.updateUI();
		window.addKeyListener(game_panel);
		
		(new Thread(this)).run();
	}
	
	/**
	 * Gibt eine Nachricht (MessageBox aus)
	 * @param msg Nachricht
	 */
	public void ShowMessage(String msg)
	{
		new MessageBox(window, msg, false);
	}
	

	@Override public void run()
	{
        long start_time, end_time, wait_time, time;

        while(true)
        {
            start_time = System.currentTimeMillis();
            game.Run();
            game_panel.updateUI();
            end_time = System.currentTimeMillis();
            time = end_time - start_time;
            wait_time = Math.max(0, MIN_TICK_TIME - time);
            try
            {
                Thread.sleep(wait_time);
            }
            catch (InterruptedException ex)
            {
            	break;
            }
        }
	}
}
