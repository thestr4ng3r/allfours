package allfours;


/**
 * Klasse für eine Karte
 */
public class Card
{
    public static enum Suit {SPADES, HEARTS, DIAMONDS, CLUBS};
    public static enum Rank {TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE};

    /**
     * Farbe
     */
    public Suit suit;
    
    /**
     * Zahl
     */
    public Rank rank;
    
    
    Card(Card.Suit s, Card.Rank r)
    {
        suit = s;
        rank = r;
    }

    Card()
    {
        suit = Suit.SPADES;
        rank = Rank.TWO;
    }

    int GetValue()
    {
        return GetValue(rank);
    }

    String RankString()
    {
        return StringByRank(rank);
    }

    String SuitString()
    {
        return StringBySuit(suit);
    }

    String CardString()
    {
        return StringByCard(this);
    }

    /*void GiveToPlayer(Player p)
    {
        p.AddCard(this);
        given_away = true;
    }*/

    public static int IDByRank(Card.Rank v)
    {
        switch(v)
        {
            case TWO:       return 0;
            case THREE:     return 1;
            case FOUR:      return 2;
            case FIVE:      return 3;
            case SIX:       return 4;
            case SEVEN:     return 5;
            case EIGHT:     return 6;
            case NINE:      return 7;
            case TEN:       return 8;
            case JACK:      return 9;
            case QUEEN:     return 10;
            case KING:      return 11;
            case ACE:       return 12;
        }
        return 0;
    }
    
    // Sepp depp h�nadreck
    
    // Hallo Herr Steiner!

    public static Card.Rank RankByID(int v)
    {
        switch(v)
        {
            case 0:         return Card.Rank.TWO;
            case 1:         return Card.Rank.THREE;
            case 2:         return Card.Rank.FOUR;
            case 3:         return Card.Rank.FIVE;
            case 4:         return Card.Rank.SIX;
            case 5:         return Card.Rank.SEVEN;
            case 6:         return Card.Rank.EIGHT;
            case 7:         return Card.Rank.NINE;
            case 8:         return Card.Rank.TEN;
            case 9:         return Card.Rank.JACK;
            case 10:        return Card.Rank.QUEEN;
            case 11:        return Card.Rank.KING;
            case 12:        return Card.Rank.ACE;
        }
        return Card.Rank.TWO;
    }

    public static Card.Suit SuitByID(int v)
    {
        switch(v)
        {
        	case 0:         return Card.Suit.CLUBS;
            case 1:         return Card.Suit.DIAMONDS;
            case 2:         return Card.Suit.HEARTS;
            case 3:         return Card.Suit.SPADES;
        }
        return Card.Suit.SPADES;
    }

    public static int IDBySuit(Card.Suit v)
    {
        switch(v)
        {
            case SPADES:    return 3;
            case HEARTS:    return 2;
            case DIAMONDS:  return 1;
            case CLUBS:     return 0;
        }
        return 0;
    }

    public static String StringByRank(Card.Rank v)
    {
        switch(v)
        {
            case TWO:       return "2";
            case THREE:     return "3";
            case FOUR:      return "4";
            case FIVE:      return "5";
            case SIX:       return "6";
            case SEVEN:     return "7";
            case EIGHT:     return "8";
            case NINE:      return "9";
            case TEN:       return "10";
            case JACK:      return "Jack";
            case QUEEN:     return "Queen";
            case KING:      return "King";
            case ACE:       return "Ace";
        }
        return "";
    }

    public static String StringBySuit(Card.Suit s)
    {
        switch(s)
        {
            case SPADES:        return "Spades";
            case HEARTS:        return "Hearts";
            case DIAMONDS:      return "Diamonds";
            case CLUBS:         return "Clubs";
        }
        return "";
    }

    public static String StringByCard(Card c)
    {
        return c.RankString() + " of " + c.SuitString();
    }

    public static int GetValue(Card.Rank v)
    {
        switch(v)
        {
            case TWO:       return 0;
            case THREE:     return 0;
            case FOUR:      return 0;
            case FIVE:      return 0;
            case SIX:       return 0;
            case SEVEN:     return 0;
            case EIGHT:     return 0;
            case NINE:      return 0;
            case TEN:       return 10;
            case JACK:      return 1;
            case QUEEN:     return 2;
            case KING:      return 3;
            case ACE:       return 4;
        }
        return 0;
    }
   
    
    public int GetCardRank(Suit trump)
    {
    	int v = 0;
    	switch(rank)
        {
	        case TWO:       v = 0;		break;
	        case THREE:     v = 1;		break;
	        case FOUR:      v = 2;		break;
	        case FIVE:      v = 3;		break;
	        case SIX:       v = 4;		break;
	        case SEVEN:     v = 5;		break;
	        case EIGHT:     v = 6;		break;
	        case NINE:      v = 7;		break;
	        case TEN:       v = 8;		break;
	        case JACK:      v = 9;		break;
	        case QUEEN:     v = 10;		break;
	        case KING:      v = 11;		break;
	        case ACE:       v = 12;		break;
	    }
    	if(suit == trump)
    		v+=13;
	    return v;
    }
}
