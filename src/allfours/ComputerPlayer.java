package allfours;

import java.util.Iterator;

import allfours.Card.Suit;

public class ComputerPlayer extends Player
{

	public ComputerPlayer(Game game)
	{
		super(game);
	}

	@Override public String GetName()
	{
		return "Computer" + (game.GetPlayers()[0] == this ? " 1" : " 2");
	}
	
	private Card GetWorstCard()
	{
		Iterator<Card> i;
		Card c;
		Card best_card = null;
		int best_card_rank;
		int temp;
		
		// kleinste karte mit der gleichen farbe
		best_card = null;
		best_card_rank = Integer.MAX_VALUE;
		i = hand_cards.iterator();
		while(i.hasNext())
		{
			c = i.next();
			
			if((temp = c.GetCardRank(game.GetTrumpSuit())) < best_card_rank && c.suit == game.GetLyingCard().suit)
			{
				best_card = c;
				best_card_rank = temp;
			}
		}
		
		if(best_card != null)
			return best_card;
		else if(game.GetLyingCard().suit != game.GetTrumpSuit()) // wenn keine karte mit der gleichen farbe existiert und die liegende kein trumpf ist
		{
			best_card = null;
			best_card_rank = Integer.MAX_VALUE;
			i = hand_cards.iterator();
			while(i.hasNext())
			{
				c = i.next();
				
				if((temp = c.GetCardRank(game.GetTrumpSuit())) < best_card_rank && c.suit == game.GetTrumpSuit()) // kleinster trumpf
				{
					best_card = c;
					best_card_rank = temp;
				}
			}
			if(best_card != null)
				return best_card;
		}
		
		// kleinste karte allgemein
		best_card = null;
		best_card_rank = Integer.MAX_VALUE;
		i = hand_cards.iterator();
		while(i.hasNext())
		{
			c = i.next();
			
			if((temp = c.GetCardRank(game.GetTrumpSuit())) < best_card_rank)
			{
				best_card = c;
				best_card_rank = temp;
			}
		}
		
		return best_card;
	}
	
	protected Card GetPlayOutCard()
	{
		Iterator<Card> i;
		Card c;
		Card best_card = null;
		int best_card_rank;
		int temp;
		
		if(game.GetLyingCard() != null) // wenn schon eine karte gelegt ist
		{
			if(Card.GetValue(game.GetLyingCard().rank) == 0)
			{
				return GetWorstCard();
			}
			else
			{
				/*also hier muss jetzt die farbe der liegende karte mit der der karten auf der
				hand verglichen werden, und dann muss die karte die die karte stechen soll gr��er sein
				und die gleiche farbe sein. fals keien soclhe akrte vorhanden dann muss mit dem
				niedrigst m�glichen trumph gestochen werden!*/
				
				// erst versuchen, mit der kleinstmöglichen Karte der gleichen farbe zu stechen
				best_card = null;
				best_card_rank = Integer.MAX_VALUE;
				i = hand_cards.iterator();
				while(i.hasNext())
				{
					c = i.next();
					temp = c.GetCardRank(game.GetTrumpSuit());
					if(c.suit == game.GetLyingCard().suit && temp < best_card_rank && temp > game.GetLyingCard().GetCardRank(game.GetTrumpSuit()))
					{
						best_card = c;
						best_card_rank = temp;
					}
					
				}
				
				if(best_card != null)
					return best_card;
				
				if(game.GetLyingCard().suit != game.GetTrumpSuit()) // wenn es kein trumpf ist, versuchen mit dem kleinsten trumpf zu sticken
				{
					best_card = null;
					best_card_rank = Integer.MAX_VALUE;
					i = hand_cards.iterator();
					while(i.hasNext())
					{
						c = i.next();
						temp = c.GetCardRank(game.GetTrumpSuit());
						if(c.suit == game.GetTrumpSuit() && temp < best_card_rank)
						{
							best_card = c;
							best_card_rank = temp;
						}
						
					}
					
					if(best_card != null)
						return best_card;
				}
				
				return GetWorstCard();
			}
		}
		else // wenn noch keine karte liegt
		{
			return hand_cards.getFirst();
		}
	}

	@Override public int AcceptTrump(Suit trump)
	{
		Iterator<Card> i;
		Card c;
		int count = 0;
		
		i = hand_cards.iterator();
		while(i.hasNext())
		{
			c = i.next();
			if(c.suit == trump)
				count++;
		}
		
		if(count > 2)
			return 1;
		else
			return -1;
	}

	@Override public int ForceTrump(Suit trump)
	{
		Iterator<Card> i;
		Card c;
		int count = 0;
		
		i = hand_cards.iterator();
		while(i.hasNext())
		{
			c = i.next();
			if(c.suit == trump)
				count++;
		}
		
		if(count >= 4)
			return 1;
		else
			return -1;
	}
	
	private void RemoveWorstCard()
	{
		Card best_card = null;
		Iterator<Card> i = hand_cards.iterator();
		Card c;
		
		while(i.hasNext())
		{
			c = i.next();
			if(best_card == null)
				best_card = c;
			else if(best_card.GetCardRank(game.GetTrumpSuit()) < c.GetCardRank(game.GetTrumpSuit()))
				best_card = c;
		}
		
		if(best_card != null)
		{
			hand_cards.remove(best_card);
			game.PutCardToTrashStack(best_card);
		}
	}

	@Override public void RemoveSpareCards()
	{
		while(hand_cards.size() > 6)
			RemoveWorstCard();
	}
}

