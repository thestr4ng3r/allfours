package allfours;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Klasse zum Laden der benoetigten Bilder
 */
public class GameData
{
	public Image test_image;
	public Image[][] card_images;
	public Image card_back_image;
	public Image background_image;
	public Image[] suit_images;
	public Image thumb_up_image, thumb_down_image;
	public Image continue_image;
	public Image multiplayer_image, singleplayer_image;
	public static final int CARD_WIDTH = 69;
	public static final int CARD_HEIGHT = 100;
	
	/**
	 * Laedt alle Bilder automatisch
	 * @param data_path Pfad, in dem sich die Bilder befinden
	 * @throws IOException falls ein Fehler auftritt
	 */
	public GameData(String data_path) throws IOException
	{
		test_image = ImageIO.read(new File(data_path + "/icon.png"));
		int x, y, s, r, k;
		BufferedImage image = ImageIO.read(new File(data_path + "/cards.png"));
		
		card_images = new Image[4][];
		for(s=0; s<4; s++)
		{
		    card_images[s] = new Image[13];
		    y = s * CARD_HEIGHT;
		    for(r=0; r<13; r++)
		    {
		        if(r == 12)
		            k = 0;
		        else
		            k = r + 1;
		        x = k * CARD_WIDTH;
		        card_images[s][r] = image.getSubimage(x, y, CARD_WIDTH, CARD_HEIGHT);
		    }
		}
		
		card_back_image = image.getSubimage(2*CARD_WIDTH, 4*CARD_HEIGHT, CARD_WIDTH, CARD_HEIGHT);
		
		image = ImageIO.read(new File(data_path + "/suits.png"));
		suit_images = new Image[4];
		
		for(s=0; s<4; s++)
			suit_images[s] = image.getSubimage((s%2) * image.getWidth() / 2, (int)Math.floor(s/2) * image.getHeight() / 2, image.getWidth() / 2, image.getHeight() / 2);
		
		background_image = ImageIO.read(new File(data_path + "/bg.png"));
		
		thumb_up_image = ImageIO.read(new File(data_path + "/thumb_up.png"));
		thumb_down_image = ImageIO.read(new File(data_path + "/thumb_down.png"));
		
		continue_image = ImageIO.read(new File(data_path + "/continue.png"));
		
		singleplayer_image = ImageIO.read(new File(data_path + "/singleplayer.png"));
		multiplayer_image = ImageIO.read(new File(data_path + "/multiplayer.png"));
	}
		
	public Image GetCardImage(Card.Suit suit, Card.Rank rank)
	{
		return card_images[Card.IDBySuit(suit)][Card.IDByRank(rank)];
	}
	
	public Image GetCardImage(Card c)
	{
		return GetCardImage(c.suit, c.rank);
	}
	
	
	
}
