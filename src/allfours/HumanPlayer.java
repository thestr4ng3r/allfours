package allfours;

import java.util.Iterator;

import allfours.Card.Suit;

public class HumanPlayer extends Player
{
	private Card card_to_put;
	private boolean turn;
	
	public HumanPlayer(Game game)
	{
		super(game);
		
		card_to_put = null;
		turn = false;
	}

	@Override public String GetName()
	{
		return "Human" + (game.GetPlayers()[0] == this ? " 1" : " 2");
	}
	
	protected Card GetPlayOutCard()
	{
		if(card_to_put == null)
		{
			turn = true;
			return null;
		}
		else
		{
			turn = false;
			Card c = card_to_put;
			card_to_put = null;
			return c;
		}
	}
	
	public void SetCardToPut(Card c)
	{
		if(turn)
		{
			Iterator<Card> i = hand_cards.iterator();
			boolean p = false;
			Card ic;
			while(i.hasNext())
			{
				ic = i.next();
				if(ic.suit == game.GetTrumpSuit() || game.GetLyingCard() == null)
				{
					p = true;
					break;
				}
				else if(ic.suit == game.GetLyingCard().suit)
				{
					p = true;
					break;
				}			
			}
			
			if(c.suit == game.GetTrumpSuit() || game.GetLyingCard() == null || !p)
				card_to_put = c;
			else if(c.suit == game.GetLyingCard().suit)
				card_to_put = c;
			else
				game.GetProgram().ShowMessage("This is neither the lying suit nor a trump");		
		}
	}

	public void RemoveSpareCard(Card c)
	{
		if(hand_cards.remove(c))
			game.PutCardToTrashStack(c);
	}
	
	@Override public int AcceptTrump(Suit trump)
	{
		return 0;
	}

	@Override public int ForceTrump(Suit trump)
	{
		return 0;
	}

	@Override public void RemoveSpareCards()
	{
	}
}
