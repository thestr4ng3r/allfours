package allfours.ui;

/**
 * Interface zum reagieren auf einfache Klicks
 */
public interface ClickListener
{
	/**
	 * Callback für einen Mausklick
	 * @param button Mausbutton
	 */
	public void OnClick(int button);
}
