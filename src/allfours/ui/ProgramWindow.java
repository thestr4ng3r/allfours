package allfours.ui;

import java.awt.Image;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import allfours.AllFoursProgram;

/**
 * Klasse für das Programmfenster
 */
public class ProgramWindow extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	//private AllFoursProgram program;
	public Image icon;
	
	
	public ProgramWindow(AllFoursProgram p)
	{
		super("All Fours");
		try
		{
			icon = ImageIO.read(new File("data/icon.png"));
			setIconImage(icon);
		}
		catch(IOException e)
		{
			
		}
		
		setSize(900, 600);
		
		addWindowListener(new WindowListener()
		{
			@Override public void windowOpened(WindowEvent arg0) { }
			@Override public void windowIconified(WindowEvent arg0) { }
			@Override public void windowDeiconified(WindowEvent arg0) { }
			@Override public void windowDeactivated(WindowEvent arg0) { }
			@Override public void windowClosing(WindowEvent arg0) { System.exit(0); }
			@Override public void windowClosed(WindowEvent arg0) { }
			@Override public void windowActivated(WindowEvent arg0) { }
		});
		
		//program = p;
		setVisible(true);
	}
}
