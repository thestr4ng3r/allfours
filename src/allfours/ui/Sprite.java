package allfours.ui;

import java.awt.Graphics;

/**
 * Abstrakte Klasse für ein Object, das auf ein GraphicsSurface gepaintet werden kann
 *
 */
public abstract class Sprite implements Comparable<Sprite>
{
	protected int x, y;
	protected float z;
	protected int width, height;
	
	private boolean visible;
	
	private ClickListener click_listener;

	public Sprite()
	{
		x = y = 0;
		z = 0.0f;
		visible = true;
		click_listener = null;
	}
	
	public int GetX()		{ return x; }
	public int GetY()		{ return y; }
	public float GetZ()	{ return z; }
	public int GetWidth()	{ return width; }
	public int GetHeight()	{ return height; }
	public boolean GetVisible() { return visible; }
	
	public void SetPosition(int x, int y) { this.x = x; this.y = y; }
	public void SetX(int x)		{ this.x = x; }
	public void SetY(int y)		{ this.y = y; }
	public void SetZ(float z)		{ this.z = z; }	
	public void SetXCenter(int x) { this.x = x - width / 2; }
	public void SetYCenter(int y) { this.y = y - height / 2; }
	public void SetPositionCenter(int x, int y) { SetXCenter(x); SetYCenter(y); }
	public void SetVisible(boolean v) { visible = v; }
	public void SetClickListener(ClickListener l) { click_listener = l; }
	
	/**
	 * Ruft die OnClick Methode des entsprechenden ClickListeners auf
	 * @param button Mausbutton
	 * @param x
	 * @param y
	 */
	public void Click(int button, int x, int y)
	{
		if(click_listener == null)
			return;
		click_listener.OnClick(button);
	}
	
	/**
	 * Abstrakte Methode zum painten des Sprites
	 * @param g Context
	 * @param offset_x Verschiebung x
	 * @param offset_y Verschiebung y
	 */
	public abstract void Paint(Graphics g, int offset_x, int offset_y);
	
	public void Paint(Graphics g) { if(!visible) return ; Paint(g, 0, 0); }

	@Override public int compareTo(Sprite other)
	{		
		if(z > other.GetZ())
			return 1;
		if(z < other.GetZ())
			return -1;
		else
			return 0;
	}
}
