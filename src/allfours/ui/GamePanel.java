package allfours.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import allfours.Card;
import allfours.Game;
import allfours.Game.Phase;
import allfours.GameData;
import allfours.HumanPlayer;
import allfours.Player;

/**
 * JPanel zur Darstellung des Spieles
 */
public class GamePanel extends DoubleBuffer implements MouseListener, KeyListener
{
	private static final long serialVersionUID = 1L;
	
	private Game game;
	private GameData data;
	private GraphicsSurface surface;
	
	private TiledSprite background;
	private ImageSprite root_stack;
	private ImageSprite uncovered_root_card_sprite;
	private ImageSprite accept_trump_sprite;
	private ImageSprite deny_trump_sprite;
	private CardHandSprite[] card_hand_sprites;
	private TextSprite[] player_message_sprites;
	private TextSprite[] player_name_sprites;
	private TextSprite[] player_points_sprites;
	private ImageSprite trump_suit_sprite;
	private ImageSprite lying_card_sprite;
	private ImageSprite lying_card2_sprite;
	private ImageSprite continue_sprite;
	private TextSprite center_message_sprite;
	private ImageSprite singleplayer_sprite, multiplayer_sprite;
	//private TextSprite trump_suit_text_sprite;
	
	public static final int BORDER = 30;
	
	public GamePanel(GameData data, Game g)
	{
		super();
		
		game = g;
		this.data = data;
		
		surface = new GraphicsSurface(0, 0, getWidth(), getHeight());
		
		background = new TiledSprite(data.background_image);
		background.SetPosition(0, 0);
		background.SetZ(-1.0f);
		surface.AddSprite(background);
		
		root_stack = new ImageSprite(data.card_back_image);
		surface.AddSprite(root_stack);
		
		uncovered_root_card_sprite = new ImageSprite();
		uncovered_root_card_sprite.SetVisible(false);
		surface.AddSprite(uncovered_root_card_sprite);
		
		accept_trump_sprite = new ImageSprite(data.thumb_up_image);
		accept_trump_sprite.SetClickListener(new ClickListener()
		{
			@Override public void OnClick(int button)
			{
				if(game.GetPhase() == Phase.ACCEPT_TRUMP)
					game.AcceptTrumpSuit();
				else if(game.GetPhase() == Phase.FORCE_TRUMP)
					game.ForceTrumpSuit();
			}
		});
		accept_trump_sprite.SetVisible(false);
		surface.AddSprite(accept_trump_sprite);
		
		deny_trump_sprite = new ImageSprite(data.thumb_down_image);
		deny_trump_sprite.SetClickListener(new ClickListener()
		{
			@Override public void OnClick(int button)
			{
				if(game.GetPhase() == Phase.ACCEPT_TRUMP)
					game.DenyTrumpSuit();
				else if(game.GetPhase() == Phase.FORCE_TRUMP)
					game.NotForceTrumpSuit();
			}
		});
		deny_trump_sprite.SetVisible(false);
		surface.AddSprite(deny_trump_sprite);
		
		card_hand_sprites = new CardHandSprite[2];
		
		card_hand_sprites[0] = new CardHandSprite(data);
		card_hand_sprites[0].SetCardClickListener(new CardClickListener()
		{
			@Override public void OnClick(Card c)
			{
				Player p = game.GetPlayers()[0];
				if(p instanceof HumanPlayer)
				{
					if(game.GetPhase() == Phase.PLAY)
						((HumanPlayer)p).SetCardToPut(c);
					else if(game.GetPhase() == Phase.REMOVE_SPARE_CARDS)
						((HumanPlayer)p).RemoveSpareCard(c);
				}
			}
		});
		
		surface.AddSprite(card_hand_sprites[0]);
		card_hand_sprites[1] = new CardHandSprite(data);
		card_hand_sprites[1].SetCardClickListener(new CardClickListener()
		{
			@Override public void OnClick(Card c)
			{
				Player p = game.GetPlayers()[1];
				if(p instanceof HumanPlayer)
				{
					if(game.GetPhase() == Phase.PLAY)
						((HumanPlayer)p).SetCardToPut(c);
					else if(game.GetPhase() == Phase.REMOVE_SPARE_CARDS)
						((HumanPlayer)p).RemoveSpareCard(c);
				}
			}
		});
		surface.AddSprite(card_hand_sprites[1]);
		
		lying_card_sprite = new ImageSprite();
		lying_card_sprite.SetVisible(false);
		surface.AddSprite(lying_card_sprite);
		
		lying_card2_sprite = new ImageSprite();
		lying_card2_sprite.SetVisible(false);
		surface.AddSprite(lying_card2_sprite);
		
		player_name_sprites = new TextSprite[2];
		
		player_name_sprites[0] = new TextSprite();
		player_name_sprites[0].SetColor(Color.yellow);
		surface.AddSprite(player_name_sprites[0]);
		player_name_sprites[1] = new TextSprite();
		player_name_sprites[1].SetColor(Color.yellow);
		surface.AddSprite(player_name_sprites[1]);

		player_points_sprites = new TextSprite[2];
		player_points_sprites[0] = new TextSprite();
		player_points_sprites[0].SetColor(Color.yellow);
		surface.AddSprite(player_points_sprites[0]);
		player_points_sprites[1] = new TextSprite();
		player_points_sprites[1].SetColor(Color.yellow);
		surface.AddSprite(player_points_sprites[1]);
		
		player_message_sprites = new TextSprite[2];
		
		player_message_sprites[0] = new TextSprite();
		player_message_sprites[0].SetColor(Color.yellow);
		surface.AddSprite(player_message_sprites[0]);
		player_message_sprites[1] = new TextSprite();
		player_message_sprites[1].SetColor(Color.yellow);
		surface.AddSprite(player_message_sprites[1]);
		
		center_message_sprite = new TextSprite();
		center_message_sprite.SetColor(Color.yellow);
		surface.AddSprite(center_message_sprite);
		
		trump_suit_sprite = new ImageSprite();
		surface.AddSprite(trump_suit_sprite);
		
		continue_sprite = new ImageSprite(data.continue_image);
		continue_sprite.SetClickListener(new ClickListener()
		{
			@Override public void OnClick(int button)
			{
				game.Continue();
			}
		});
		surface.AddSprite(continue_sprite);
		
		singleplayer_sprite = new ImageSprite(data.singleplayer_image);
		singleplayer_sprite.SetClickListener(new ClickListener()
		{
			@Override public void OnClick(int button)
			{
				game.StartSinglePlayer();
			}
		});
		surface.AddSprite(singleplayer_sprite);
		
		multiplayer_sprite = new ImageSprite(data.multiplayer_image);
		multiplayer_sprite.SetClickListener(new ClickListener()
		{
			@Override public void OnClick(int button)
			{
				game.StartMultiPlayer();
			}
		});
		surface.AddSprite(multiplayer_sprite);
				
		addMouseListener(this);
	}
	
	/**
	 * Berechnet die Positionen, etc. der Sprites
	 */
	public void RefreshUI()
	{
		surface.SetBounds(0, 0, getWidth(), getHeight());
		background.SetBounds(getWidth(), getHeight());
		
		if(game.GetPhase() == Phase.START_GAME)
		{
			singleplayer_sprite.SetXCenter(getWidth() / 2);
			singleplayer_sprite.SetYCenter(getHeight() / 2 - 50);
			singleplayer_sprite.SetVisible(true);
			multiplayer_sprite.SetXCenter(getWidth() / 2);
			multiplayer_sprite.SetYCenter(getHeight() / 2 + 50);
			multiplayer_sprite.SetVisible(true);
			uncovered_root_card_sprite.SetVisible(false);
			accept_trump_sprite.SetVisible(false);
			deny_trump_sprite.SetVisible(false);
			trump_suit_sprite.SetVisible(false);
			card_hand_sprites[0].SetVisible(false);
			card_hand_sprites[1].SetVisible(false);
			lying_card_sprite.SetVisible(false);
			lying_card2_sprite.SetVisible(false);
			player_name_sprites[0].SetVisible(false);
			player_name_sprites[1].SetVisible(false);
			player_points_sprites[0].SetVisible(false);
			player_points_sprites[1].SetVisible(false);
			player_message_sprites[0].SetVisible(false);
			player_message_sprites[1].SetVisible(false);
			continue_sprite.SetVisible(false);
			center_message_sprite.SetVisible(false);
			root_stack.SetVisible(false);
		}
		else
		{	
			root_stack.SetX(BORDER);
			root_stack.SetYCenter(getHeight() / 2);
			root_stack.SetVisible(true);
			
			singleplayer_sprite.SetVisible(false);
			multiplayer_sprite.SetVisible(false);
	
			uncovered_root_card_sprite.SetVisible(game.GetUncoveredRootCard() != null && !game.GetWaitForContinue());
			accept_trump_sprite.SetVisible(game.GetUncoveredRootCard() != null && !game.GetWaitForContinue());
			deny_trump_sprite.SetVisible(game.GetUncoveredRootCard() != null && !game.GetWaitForContinue());
			if(game.GetUncoveredRootCard() != null)
				uncovered_root_card_sprite.SetImage(data.GetCardImage(game.GetUncoveredRootCard()));
			
			if(game.GetPhase() != Game.Phase.ACCEPT_TRUMP && game.GetPhase() != Game.Phase.FORCE_TRUMP)
			{
				trump_suit_sprite.SetImage(data.suit_images[Card.IDBySuit(game.GetTrumpSuit())]);
				trump_suit_sprite.SetX(getWidth() - BORDER - trump_suit_sprite.GetWidth());
				trump_suit_sprite.SetYCenter(getHeight() / 2);
				trump_suit_sprite.SetVisible(true);
			}
			else
				trump_suit_sprite.SetVisible(false);
			
			uncovered_root_card_sprite.SetX(root_stack.GetX() + root_stack.GetWidth() + 10);
			uncovered_root_card_sprite.SetY(root_stack.GetY());
			accept_trump_sprite.SetX(uncovered_root_card_sprite.GetX());
			accept_trump_sprite.SetY(uncovered_root_card_sprite.GetY() + uncovered_root_card_sprite.GetHeight() + 10);
			deny_trump_sprite.SetX((uncovered_root_card_sprite.GetX()  + uncovered_root_card_sprite.GetWidth()) - deny_trump_sprite.GetWidth());
			deny_trump_sprite.SetY(uncovered_root_card_sprite.GetY() + uncovered_root_card_sprite.GetHeight() + 10);
			
			card_hand_sprites[0].SetY(getHeight() - GameData.CARD_HEIGHT - BORDER * 2);
			card_hand_sprites[0].SetWidth(getWidth() - 2 * 100);
			card_hand_sprites[0].SetXCenter(getWidth() / 2);
			card_hand_sprites[0].SetVisible(true);
			card_hand_sprites[1].SetY(BORDER * 2);
			card_hand_sprites[1].SetWidth(getWidth() - 2 * 100);
			card_hand_sprites[1].SetXCenter(getWidth() / 2);
			card_hand_sprites[1].SetVisible(true);
			
	
			if(game.GetPhase() == Phase.SUMMARY || game.GetPhase() == Phase.GAME_OVER)
			{
				card_hand_sprites[0].SetCards(game.GetPlayers()[0].GetTrickCards());
				card_hand_sprites[0].SetHidden(false);
				card_hand_sprites[1].SetCards(game.GetPlayers()[1].GetTrickCards());
				card_hand_sprites[1].SetHidden(false);
			}
			else
			{
				if(!game.GetMultiPlayer())
				{
					card_hand_sprites[0].SetHidden(!(game.GetPlayers()[0] instanceof HumanPlayer));
					card_hand_sprites[1].SetHidden(!(game.GetPlayers()[1] instanceof HumanPlayer));
				}
				else
				{
					card_hand_sprites[0].SetHidden(!(game.GetPlayerTurn() == 0 && !game.GetWaitForContinue()));
					card_hand_sprites[1].SetHidden(!(game.GetPlayerTurn() == 1 && !game.GetWaitForContinue()));
				}
				card_hand_sprites[0].SetCards(game.GetPlayers()[0].GetCards());
				card_hand_sprites[1].SetCards(game.GetPlayers()[1].GetCards());
			}
			
			lying_card_sprite.SetVisible(game.GetLyingCard() != null && game.GetCenterMessage().length() == 0);
			if(game.GetLyingCard() != null)
				lying_card_sprite.SetImage(data.GetCardImage(game.GetLyingCard()));
			lying_card_sprite.SetPositionCenter(getWidth() / 2 - 10, getHeight() / 2 - 10);
			
			lying_card2_sprite.SetVisible(game.GetLyingCard2() != null && game.GetCenterMessage().length() == 0);
			if(game.GetLyingCard2() != null)
				lying_card2_sprite.SetImage(data.GetCardImage(game.GetLyingCard2()));
			lying_card2_sprite.SetPositionCenter(getWidth() / 2 + 10, getHeight() / 2 + 10);
			
			player_name_sprites[0].SetText(game.GetPlayers()[0].GetName());
			player_name_sprites[0].SetPosition(BORDER, getHeight() - BORDER);
			player_name_sprites[0].SetColor(game.GetCurrentPlayer() == 0 ? Color.yellow : Color.darkGray);
			player_name_sprites[0].SetVisible(true);
			player_name_sprites[1].SetText(game.GetPlayers()[1].GetName());
			player_name_sprites[1].SetPosition(BORDER, BORDER);
			player_name_sprites[1].SetColor(game.GetCurrentPlayer() == 1 ? Color.yellow : Color.darkGray);
			player_name_sprites[1].SetVisible(true);
			
			player_points_sprites[0].SetText("" + game.GetPlayers()[0].GetPoints());
			player_points_sprites[0].SetPosition(getWidth() - 50, getHeight() - 50);
			player_points_sprites[0].SetVisible(true);
			player_points_sprites[1].SetText("" + game.GetPlayers()[1].GetPoints());
			player_points_sprites[1].SetPosition(getWidth() - 50, 50);
			player_points_sprites[1].SetVisible(true);
			
			if(game.GetPhase() == Game.Phase.PLAY_FINISHED)
			{
				player_message_sprites[0].SetVisible(true);
				player_message_sprites[0].SetText(game.GetWinner() == 0 ? "Winner" : "Loser");
				player_message_sprites[0].SetPosition(getWidth() / 2, card_hand_sprites[0].GetY() - BORDER);
				player_message_sprites[0].SetHorizontalAlignment(TextSprite.HorizontalAlignment.CENTER);
				player_message_sprites[1].SetVisible(true);
				player_message_sprites[1].SetText(game.GetWinner() == 1 ? "Winner" : "Loser");
				player_message_sprites[1].SetPosition(getWidth() / 2, card_hand_sprites[1].GetY() + card_hand_sprites[1].GetHeight() + BORDER);
				player_message_sprites[1].SetHorizontalAlignment(TextSprite.HorizontalAlignment.CENTER);
			}
			else
			{
				player_message_sprites[0].SetVisible(false);
				player_message_sprites[1].SetVisible(false);
			}
			
			continue_sprite.SetPosition(getWidth() - continue_sprite.GetWidth() - 70, getHeight() - continue_sprite.GetHeight() - BORDER);
			continue_sprite.SetVisible(game.GetWaitForContinue());
			
			center_message_sprite.SetPosition(getWidth() / 2, getHeight() / 2);
			center_message_sprite.SetHorizontalAlignment(TextSprite.HorizontalAlignment.CENTER);
			center_message_sprite.SetText(game.GetCenterMessage());
			center_message_sprite.SetVisible(true);
		}
	}

	@Override public void PaintBuffer(Graphics g)
	{
		RefreshUI();
		surface.Paint(g);
	}

	@Override public void mouseClicked(MouseEvent e)
	{
		surface.Click(e.getX(), e.getY(), e.getButton());
	}
	
	
	@Override public void mouseEntered(MouseEvent e) { }
	@Override public void mouseExited(MouseEvent e) { }
	@Override public void mousePressed(MouseEvent e) { }
	@Override public void mouseReleased(MouseEvent e) { }

	@Override public void keyPressed(KeyEvent e)
	{
		if(game.GetPhase() != Phase.START_GAME)
			return;
		
		if(e.getKeyChar() == 'm')
			game.StartMultiPlayer();
		else if(e.getKeyChar() == 's')
			game.StartSinglePlayer();
	}

	@Override public void keyReleased(KeyEvent e) { }
	@Override public void keyTyped(KeyEvent e) { }
}
