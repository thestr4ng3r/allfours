package allfours.ui;

import java.awt.*;
import java.awt.event.*;

/**
 * Klasse für Message Boxen
 */
public class MessageBox extends Dialog implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private Button ok,can;
    public boolean isOk = false;

    /**
     * @param frame   parent frame
     * @param msg     message
     * @param okcan   true : ok und cancel, false : nur ok
     */
    public MessageBox(Frame frame, String msg, boolean okcan)
    {
        super(frame, "Message", true);
        setLayout(new BorderLayout());
        add("Center",new Label(msg));
        AddOKCancelPanel(okcan);
        CreateFrame();
        pack();
        setVisible(true);
    }
    
    /**
     * @param frame   parent frame
     * @param msg     message
     */
    public MessageBox(Frame frame, String msg)
    {
        this(frame, msg, false);
    }

    private void AddOKCancelPanel(boolean okcan)
    {
        Panel p = new Panel();
        p.setLayout(new FlowLayout());
        CreateOKButton(p);
        if(okcan)
            CreateCancelButton(p);
        add("South",p);
    }

    private void CreateOKButton(Panel p)
    {
        p.add(ok = new Button("OK"));
        ok.addActionListener(this);
    }

    private void CreateCancelButton(Panel p)
    {
        p.add(can = new Button("Cancel"));
        can.addActionListener(this);
    }

    private void CreateFrame()
    {
        Dimension d = getToolkit().getScreenSize();
        setLocation(d.width/3,d.height/3);
    }

    public void actionPerformed(ActionEvent ae)
    {
        if(ae.getSource() == ok)
        {
            isOk = true;
            setVisible(false);
        }
        else if(ae.getSource() == can)
            setVisible(false);
    }
}
