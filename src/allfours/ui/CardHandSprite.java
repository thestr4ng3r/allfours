package allfours.ui;

import java.awt.Graphics;
import java.awt.Image;

import allfours.Card;
import allfours.GameData;

/**
 * Sprite zur Darstellung von mehreren Karten
 */
public class CardHandSprite extends Sprite
{
	private Card[] cards;
	private boolean hidden;
	private GameData data;
	private CardClickListener card_click_listener;
	public static final int MAX_CARD_DIST = 20;
	
	public CardHandSprite(GameData data)
	{
		super();
		card_click_listener = null;
		this.data = data;
		cards = new Card[0];
		hidden = true;
		height = GameData.CARD_HEIGHT;
		width = GameData.CARD_WIDTH * 6;
	}
	
	public void SetHidden(boolean hidden)		{ this.hidden = hidden; }
	public void SetWidth(int width)			{ this.width = Math.min(width, GetMaxWidth()); }
	public void SetCards(Card[] cards)			{ this.cards = cards; }
	public void SetCardClickListener(CardClickListener listener) { card_click_listener = listener; }
	
	/**
	 * Erweiterung von Sprite.Click() um das Karten-bezogene klicken
	 */
	@Override public void Click(int button, int x, int y)
	{
		super.Click(button, x, y);
		if(card_click_listener != null && !hidden)
		{
			int cx;
			for(int i=0; i<cards.length; i++)
			{
				cx = GetCardX(i);
				if(x >= cx && x < cx + GameData.CARD_WIDTH)
				{
					card_click_listener.OnClick(cards[i]);
					break;
				}
			}
		}
	}

	@Override public void Paint(Graphics g, int offset_x, int offset_y)
	{
		if(cards.length == 0)
			return;
		
		int i;
		Image img = data.card_back_image;
		
		for(i=0; i<cards.length; i++)
		{
			if(!hidden)
				img = data.GetCardImage(cards[i]);
			g.drawImage(img, x + GetCardX(i) + offset_x, y + offset_y, null); 
		}
	}
	
	/**
	 * @return maximale Breite des Sprites
	 */
	public int GetMaxWidth()
	{
		return cards.length * GameData.CARD_WIDTH + Math.max(0, cards.length - 1) * MAX_CARD_DIST;
	}
	
	/**
	 * Berechnet die horizontale Position einer Karte
	 * @param card Index der Karte
	 * @return x-Position
	 */
	public int GetCardX(int card)
	{
		if(cards.length <= 1)
			return width / 2 - GameData.CARD_WIDTH / 2;	
		
		int max_width = GetMaxWidth();
		int shift = 0;
		int dist;
		
		if(width > max_width)
		{
			shift = (width - max_width) / 2;
			dist = GameData.CARD_WIDTH + MAX_CARD_DIST;
		}
		else
			dist = (width - GameData.CARD_WIDTH) / (cards.length - 1);
		
		return shift + dist * card;
	}
}
