package allfours.ui;

import allfours.Card;

/**
 * Interface für Klicks auf Karten
 */
public interface CardClickListener
{
	/**
	 * Callback für einen Klick auf eine Karte
	 * @param c Karte
	 */
	public void OnClick(Card c);
}
