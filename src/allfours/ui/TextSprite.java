package allfours.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

/**
 * Sprite zur Darstellung von Text
 */
public class TextSprite extends Sprite
{
	private String text;
	private Font font;
	private Color color;
	
	public enum HorizontalAlignment { LEFT, CENTER, RIGHT };
	private HorizontalAlignment halign;
	
	public TextSprite()
	{
		super();
		font = new Font("Ubuntu", Font.BOLD, 16);
		color = Color.black;
		height = 16;
		halign = HorizontalAlignment.LEFT;
	}
	
	public TextSprite(String text)
	{
		super();
		font = new Font("Ubuntu", Font.BOLD, 16);
		height = 16;
		color = Color.black;
		SetText(text);
		halign = HorizontalAlignment.LEFT;
	}
	
	public void SetText(String text)		{ this.text = text; }
	public void SetColor(Color color)		{ this.color = color; }
	public void SetHorizontalAlignment(HorizontalAlignment align) { halign = align; }
	
	@Override public void Paint(Graphics g, int offset_x, int offset_y)
	{
		Rectangle2D bounds = g.getFontMetrics(font).getStringBounds(text, g);
		int xpos = offset_x + x;
		
		switch(halign)
		{
			case CENTER:	xpos -= bounds.getWidth() / 2; break;
			case RIGHT:		xpos -= bounds.getWidth(); break;
		}
		
		g.setFont(font);
		g.setColor(color);
		g.drawString(text, xpos, offset_y + y);
	}

}
