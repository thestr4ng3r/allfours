package allfours.ui;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * Erweiterung von JPanel um Double Buffering
 */
public abstract class DoubleBuffer extends JPanel
{
	private static final long serialVersionUID = 1L;
	private int bufferWidth;
	private int bufferHeight;
	private Image bufferImage;
	private Graphics bufferGraphics;
	
	public DoubleBuffer()
	{
		super();
	}
	
	@Override public void update(Graphics g)
	{
		paint(g);
	}
	
	@Override public void paint(Graphics g)
	{
		if(bufferWidth!=getSize().width || bufferHeight!=getSize().height || 
				bufferImage==null || bufferGraphics==null)
			resetBuffer();
	
	    if(bufferGraphics!=null)
	    {
	        bufferGraphics.clearRect(0,0,bufferWidth,bufferHeight);
	        PaintBuffer(bufferGraphics);
	        g.drawImage(bufferImage,0,0,this);
	    }
	}
	
	/**
	 * abstrakte Methode zum beschreiben des Buffers
	 */
	public abstract void PaintBuffer(Graphics g);
	
	private void resetBuffer()
	{
		bufferWidth=getSize().width;
		bufferHeight=getSize().height;
		
		if(bufferGraphics!=null)
		{
		    bufferGraphics.dispose();
		    bufferGraphics=null;
		}
		if(bufferImage!=null)
		{
		    bufferImage.flush();
		    bufferImage=null;
		}
		System.gc();
		
		bufferImage=createImage(bufferWidth,bufferHeight);
		bufferGraphics=bufferImage.getGraphics();
	}
}
