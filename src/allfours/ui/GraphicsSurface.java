package allfours.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Klasse zur Organisation und zum painten von Sprites
 */
public class GraphicsSurface
{
	private LinkedList<Sprite> objects;
	private Color background;
	private int x, y;
	private int width, height;
	
	public GraphicsSurface(int x, int y, int width, int height)
	{
		objects = new LinkedList<Sprite>();
		background = Color.BLACK;
		SetBounds(x, y, width, height);
	}
	
	/**
	 * Legt Position und Größe fest.
	 */
	public void SetBounds(int x, int y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void SetBackground(Color c) { background = c; }
	
	public void AddSprite(Sprite s)		{ if(!objects.contains(s)) objects.add(s); }
	public void RemoveSprite(Sprite s)	{ objects.remove(s); }
	
	/**
	 * Gibt, wenn nötig einen Mausklick an die entsprechenden Sprites weiter
	 * @param x
	 * @param y
	 * @param button Mausbutton
	 */
	public void Click(int x, int y, int button)
	{
		Iterator<Sprite> i = objects.iterator();
		Sprite s;
		while(i.hasNext())
		{
			s = i.next();
			if(!s.GetVisible())
				continue;
			if(x >= s.GetX() && x <= s.GetX() + s.GetWidth()
					&& y >= s.GetY() && y <= s.GetY() + s.GetHeight())
			{
				s.Click(button, x - s.GetX(), y - s.GetY());
			}
		}
	}
	
	/**
	 * Paintet alle Sprites auf g
	 * @param g Context
	 */
	public void Paint(Graphics g)
	{
		Collections.sort(objects);
		g.setColor(background);
		g.fillRect(x, y, width, height);

		Iterator<Sprite> i = objects.iterator();
		Sprite s;
		while(i.hasNext())
		{
			s = i.next();
			if(!s.GetVisible())
				continue;
			s.Paint(g, x, y);
		}
	}
	
}
