package allfours.ui;

import java.awt.Graphics;
import java.awt.Image;

/**
 * Sprite zur Darstellung eines Image
 */
public class ImageSprite extends Sprite
{
	protected Image image;
	
	public ImageSprite()
	{
		super();
		SetImage(null);
	}
	
	public ImageSprite(Image img)
	{
		super();
		SetImage(img);
	}
	
	public void SetImage(Image img)
	{
		image = img;
		if(image != null)
		{
			width = image.getWidth(null);
			height = image.getHeight(null);
		}
		else
			width = height = 0;
	}
	
	public void Paint(Graphics g, int offset_x, int offset_y)
	{
		if(image == null)
			return;
		g.drawImage(image, x + offset_x, y + offset_y, width, height, null);
	}
}
