package allfours.ui;

import java.awt.Graphics;
import java.awt.Image;

/**
 * Sprite zur Darstellung eines gekachelten Bildes (z.B. Hintergrund)
 */
public class TiledSprite extends Sprite
{
	protected Image image;
	
	public TiledSprite(Image img)
	{
		super();
		SetImage(img);
		SetBounds(0, 0);
	}
	
	public TiledSprite()
	{
		super();
		SetBounds(0, 0);
	}

	public void SetImage(Image img)
	{
		image = img;
	}
	
	public void SetBounds(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	
	public void Paint(Graphics g, int offset_x, int offset_y)
	{
		if(image == null)
			return;
		
		int _x, _y;
		
		for(_x = x; _x < width; _x += image.getWidth(null))
			for(_y = y; _y < height; _y += image.getHeight(null))
				g.drawImage(image, _x + offset_x, _y + offset_y, image.getWidth(null), image.getHeight(null), null);
	}
}
