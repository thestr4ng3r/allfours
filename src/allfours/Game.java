package allfours;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;

import allfours.Card.Rank;

/**
 * Klasse, die ein Spiel simuliert
 */
public class Game
{
	private LinkedList<Card> root_stack;
	private LinkedList<Card> trash_stack;
	
	private Card uncovered_root_card;
	
	private int current_player;
	private Player[] player;
	private int winner;
	
	private Card.Suit trump_suit;
	
	/**
	 * Gibt den momentanen Zustand eines Spieles an
	 */
	public enum Phase { START_GAME, ACCEPT_TRUMP, FORCE_TRUMP, REMOVE_SPARE_CARDS, PLAY, PLAY_FINISHED, SUMMARY, GAME_OVER }; // ACCEPT_TRUMP -> PLAY -> PLAY_FINISHED -> PLAY -> PLAY_FINISHED -> ... -> SUMMARY -> PLAY -> ... -> GAME_OVER
	
	private Phase phase;
	
	private Card lying_card;
	private Card lying_card2;
	
	private boolean wait_for_continue;
	
	private AllFoursProgram program;
	
	private String center_message;
	
	private int game_winner;
	
	private boolean multiplayer;
	private int player_turn;
	
	private FileOutputStream fstream = null;
	
	
	public Game(AllFoursProgram program)
	{		
		this.program = program;
		try
		{
			fstream = new FileOutputStream(new File("log.txt"));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		phase = Phase.START_GAME;
	}
	
	/**
	 * Gibt einen Text aus und schreibt ihn in die Log-Datei
	 * @param text Text
	 */
	public void Log(String text)
	{
		if(fstream != null)
		{
			try
			{
				fstream.write((text + "\n").getBytes());
			}
			catch (IOException e) { }
		}
		System.out.println(text);
	}
	
	public void StartSinglePlayer()
	{
		Log("Starting Single Player Game");
		center_message = "";
		multiplayer = false;
		
		Player player1 = new HumanPlayer(this);
		Player player2 = new ComputerPlayer(this);
		
		player = new Player[2];
		player[0] = player1;
		player[1] = player2;
		root_stack = new LinkedList<Card>();
		trash_stack = new LinkedList<Card>();
		
		lying_card = null;
		
		uncovered_root_card = null;
		
		for(int r=0; r<13; r++)
			for(int s=0; s<4; s++)
				root_stack.add(new Card(Card.SuitByID(s), Card.RankByID(r)));
		
		StartRound();
	}
	
	public void StartMultiPlayer()
	{
		Log("Starting Multi Player Game");
		center_message = "";
		multiplayer = true;
		
		Player player1 = new HumanPlayer(this);
		Player player2 = new HumanPlayer(this);
		
		player = new Player[2];
		player[0] = player1;
		player[1] = player2;
		root_stack = new LinkedList<Card>();
		trash_stack = new LinkedList<Card>();
		
		lying_card = null;
		
		uncovered_root_card = null;
		
		for(int r=0; r<13; r++)
			for(int s=0; s<4; s++)
				root_stack.add(new Card(Card.SuitByID(s), Card.RankByID(r)));
		
		StartRound();
	}
	
	
	public void StartRound()
	{
		Log("Starting round");
		Reshuffle();
		
		player_turn = current_player = Math.random() > 0.5 ? 1 : 0;
		Log("Current Player is " + current_player);
		
		for(int r=0; r<2; r++)
			for(int s=0; s<6; s++)
				player[r].TakeCard(root_stack.pop()); 
		// jeder erhaelt 6 KArten 
		
		uncovered_root_card = root_stack.pop();
		lying_card = null;
		wait_for_continue = false;
		
		phase = Phase.ACCEPT_TRUMP;
		center_message = "";
		
		if(multiplayer)
			ChangePlayer();
	}
	
	private void Reshuffle()
	{
		root_stack.addAll(trash_stack);
		trash_stack.clear();
		root_stack.addAll(player[0].GetCardsList());
		player[0].GetCardsList().clear();
		root_stack.addAll(player[1].GetCardsList());
		player[1].GetCardsList().clear();
		root_stack.addAll(player[0].GetTrickCardsList());
		player[0].GetTrickCardsList().clear();
		root_stack.addAll(player[1].GetTrickCardsList());
		player[1].GetTrickCardsList().clear();
		Collections.shuffle(root_stack);
	}
	
	/**
	 * Akzeptiert den Trumpf
	 */
	public void AcceptTrumpSuit()
	{
		Log("Trump is " + Card.StringBySuit(uncovered_root_card.suit));
		trump_suit = uncovered_root_card.suit;
		trash_stack.add(uncovered_root_card);
		uncovered_root_card = null;
		if(player[0].GetCardsCount() > 6)
		{
			phase = Phase.REMOVE_SPARE_CARDS;
		}
		else
		{
			phase = Phase.PLAY;
			center_message = "";
		}
	}
	
	/**
	 * Akzeptiert den Trumpf nicht
	 */
	public void DenyTrumpSuit()
	{
		Log(current_player + " denied " + Card.StringBySuit(uncovered_root_card.suit) + " as trump");
		
		phase = Phase.FORCE_TRUMP;
		center_message = "";
		
		if(multiplayer)
		{
			player_turn = 1-current_player;
			ChangePlayer();
		}
	}
	
	/**
	 * Erzwingt den Trumpf
	 */
	public void ForceTrumpSuit()
	{
		Log((1-current_player) + " enforced " + Card.StringBySuit(uncovered_root_card.suit) + " as trump and gave " + current_player + " a point");
		
		player[current_player].AddPoint();
		AcceptTrumpSuit();
		center_message = "";

		if(multiplayer)
		{
			player_turn = current_player;
			ChangePlayer();
		}
	}
	
	/**
	 * Erzwingt den Trumpf nicht
	 */
	public void NotForceTrumpSuit()
	{
		Card.Suit denied_suit = uncovered_root_card.suit;
		
		center_message = "";
		trash_stack.add(uncovered_root_card);
		uncovered_root_card = null;
		
		while(true)
		{
			if(root_stack.size() < 7)
				break;
			
			for(int r=0; r<2; r++)
				for(int s=0; s<3; s++)
					player[r].TakeCard(root_stack.pop());
			
			uncovered_root_card = root_stack.pop();
			if(uncovered_root_card.rank == Rank.JACK)
			{
				Log((1-current_player) + " uncovered a jack and thus gets an extra point");
				player[1-current_player].AddPoint();
			}
			
			if(uncovered_root_card.suit != denied_suit)
				break;
			
			trash_stack.add(uncovered_root_card);
			uncovered_root_card = null;
		}
		
		if(uncovered_root_card == null)
		{
			Reshuffle();
			StartRound();
		}
		else
			AcceptTrumpSuit();
		if(multiplayer)
		{
			player_turn = 0;
			ChangePlayer();
		}
	}
	
	public void PutCardToTrashStack(Card c)	{ trash_stack.add(c); }
	
	public AllFoursProgram GetProgram()	{ return program; } 
	public Card GetUncoveredRootCard()	{ return uncovered_root_card; }
	public Player[] GetPlayers()		{ return player; }
	public Card.Suit GetTrumpSuit()		{ return trump_suit; }
	public Phase GetPhase()				{ return phase; }
	public Card GetLyingCard()			{ return lying_card; }
	public Card GetLyingCard2()			{ return lying_card2; }
	public boolean GetWaitForContinue() { return wait_for_continue; }
	public int GetWinner()				{ return winner; }
	public int GetCurrentPlayer()		{ return current_player; }
	public String GetCenterMessage()	{ return center_message; }
	public boolean GetMultiPlayer()	{ return multiplayer; }
	public int GetPlayerTurn()			{ return player_turn; }
	
	public void Continue()				{ wait_for_continue = false; }
	
	public void Run()
	{
		int a;
		
		if(wait_for_continue)
			return;
		
		switch(phase)
		{
			case ACCEPT_TRUMP:
				if((a = player[current_player].AcceptTrump(uncovered_root_card.suit)) != 0)
				{
					if(a == -1)
						DenyTrumpSuit();
					else
						AcceptTrumpSuit();
				}
				else
					center_message = "Does " + player[current_player].GetName() + " accept this trump?";
				
				break;
			
			case FORCE_TRUMP:
				if((a = player[1-current_player].ForceTrump(uncovered_root_card.suit)) != 0)
				{
					if(a == -1)
						NotForceTrumpSuit();
					else
						ForceTrumpSuit();
				}
				else
					center_message = "Does " + player[1-current_player].GetName() + " want to enforce this trump and give " + player[current_player].GetName() + " a point?";
				
				break;
			
			case REMOVE_SPARE_CARDS:
				if(player[0].GetCardsCount() == 6 && player[1].GetCardsCount() == 6)
				{
					center_message = "";
					if(player_turn != current_player && multiplayer)
					{
						player_turn = current_player;
						ChangePlayer();
					}
					phase = Phase.PLAY;
				}
				else
				{
					center_message = "Select Cards to put aside";
					if(multiplayer)
					{
						if(player[0].GetCardsCount() > 6)
							player_turn = 0;
						else if(player_turn == 0)
						{
							player_turn = 1;
							ChangePlayer();
						}
					}
					else
					{
						player[0].RemoveSpareCards();
						player[1].RemoveSpareCards();
					}
				}
				break;
				
			case PLAY:
				PlayStep();
				break;
				
			case PLAY_FINISHED:
				player[winner].AddTrickCard(lying_card);
				player[winner].AddTrickCard(lying_card2);
				lying_card = null;
				lying_card2 = null;
				player_turn = current_player = winner;
				if(player[0].GetCardsCount() > 0)
				{
					if(multiplayer)
						ChangePlayer();
					phase = Phase.PLAY;
				}
				else
				{
					phase = Phase.SUMMARY;
					CalculatePoints();
					wait_for_continue = true;
				}
				break;
				
			case SUMMARY:
				StartRound();
				break;
			
			case GAME_OVER:
				System.exit(0);
				break;
		}
	}
	
	private void ChangePlayer()
	{
		if(!multiplayer)
			return;
		
		center_message = "It is " + player[player_turn].GetName() + "'s turn";
		wait_for_continue = true;
	}
	
	private void PlayStep()
	{
		Card ca, cb;
		
		center_message = "";
		if(player[0].GetCardsCount() > 0 || player[1].GetCardsCount() > 0) // wenn die beiden noch Karten auf der Hand haben, wird gespielt
	    {
				if(lying_card == null) // wenn noch keine karte liegt
				{
					player_turn = current_player;
					lying_card = ca = player[current_player].PlayOut(); // Vorhand spielt aus
					if(ca == null) // wenn der Spieler noch nicht bereit ist
						return;
					else
					{
						Log("Player " + current_player + " puts " + ca.CardString());
						if(multiplayer)
						{
							player_turn = 1-current_player;
							ChangePlayer();
						}
					}
				}
				else
					ca = lying_card;
				
				if(lying_card2 == null)
				{
					lying_card2 = cb = player[1-current_player].PlayOut(); // anderer Spiele gibt zu
					if(cb == null) // wenn der Spieler noch nicht bereit ist
						return;
					else
						Log("Player " + (1-current_player) + " puts " + cb.CardString());
				}
				else
					cb = lying_card2;

				if(ca.GetCardRank(trump_suit) >= cb.GetCardRank(trump_suit)) // Stich bekommt der mit der besseren Karte
	    		{
					winner = current_player;
	    			Log("Player " + current_player + " has made the trick");
	    		}
	    		else
	    		{
	    			Log("Player " + (1-current_player) + " has made the trick");
	    			winner = 1-current_player;
	    		}
				phase = Phase.PLAY_FINISHED;
				wait_for_continue = true;
		}
		else
			phase = Phase.SUMMARY;
	}
	
	public void CalculatePoints()
	{
		Card[][] trick_cards = new Card[2][];
		trick_cards[0] = player[0].GetTrickCards();
		trick_cards[1] = player[1].GetTrickCards();
		
		Card c;
		Card best = null;
		int best_player = 0;
		int i, p;
		int sum[] = new int[2];
		
		if(player[0].GetPoints() < 7 && player[1].GetPoints() < 7)
		{
			// Höchster Trumpf
			best = null;
			for(p=0; p<2; p++)
				for(i=0; i<trick_cards[p].length; i++)
				{
					if((c = trick_cards[p][i]).suit != trump_suit)
						continue;
					
					if(best != null)
						if(best.GetCardRank(trump_suit) > c.GetCardRank(trump_suit))
							continue;
					best = c;
					best_player = p;
				}
			
			if(best != null)
			{
				player[best_player].AddPoint();
				Log("Player " + best_player + " has the highest trump");
			}
		}
		
		if(player[0].GetPoints() < 7 && player[1].GetPoints() < 7)
		{
			// Niedrigster Trumpf
			best = null;
			for(p=0; p<2; p++)
				for(i=0; i<trick_cards[p].length; i++)
				{
					if((c = trick_cards[p][i]).suit != trump_suit)
						continue;
					
					if(best != null)
						if(best.GetCardRank(trump_suit) < c.GetCardRank(trump_suit))
							continue;
					best = c;
					best_player = p;
				}
			
			if(best != null)
			{
				player[best_player].AddPoint();
				Log("Player " + best_player + " has the lowest trump");
			}
		}
		
		
		if(player[0].GetPoints() < 7 && player[1].GetPoints() < 7)
		{
			// Trumpf Bube
			best = null;
			best_player = -1;
			for(p=0; p<2 && best_player == -1; p++)
				for(i=0; i<trick_cards[p].length; i++)
				{
					c = trick_cards[p][i];
					if(c.suit == trump_suit && c.rank == Rank.JACK)
					{
						player[p].SetPoints(player[p].GetPoints() + 1);
						best_player = p;
						break;
					}
				}

			if(best_player != -1)
			{
				player[best_player].AddPoint();
				Log("Player " + best_player + " has the trump jack");
			}
		}
		
		if(player[0].GetPoints() < 7 && player[1].GetPoints() < 7)
		{
			// Augenzahl
			sum[0] = sum[1] = 0;
			for(p=0; p<2; p++)
				for(i=0; i<trick_cards[p].length; i++)
					sum[p] += trick_cards[p][i].GetValue();

			if(sum[0] == sum[1])
				best_player = current_player;
			else
				best_player = sum[0] > sum[1] ? 0 : 1;
				
			player[best_player].AddPoint();
			Log("Player " + best_player + " has the highest card-sum");
		}
		
		if(player[0].GetPoints() >= 7 || player[1].GetPoints() >= 7)
		{
			game_winner = player[0].GetPoints() >= 7 ? 0 : 1;
			center_message = player[game_winner].GetName() + " has won the game.";
			Log(game_winner + " has won the game");
			phase = Phase.GAME_OVER;
		}
	}	
}
