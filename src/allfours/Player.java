package allfours;

import java.util.LinkedList;

public abstract class Player
{
	protected LinkedList<Card> hand_cards;
	protected LinkedList<Card> trick_cards;
	protected Game game;
	protected int points;
	
	
	public Player(Game game)
	{
		this.game = game;
		hand_cards = new LinkedList<Card>();
		trick_cards = new LinkedList<Card>();
		points=0;
	}
	
	public int GetCardsCount() { return hand_cards.size(); }
	
	/**
	 * Gibt dem Spieler eine Karte auf die Hand
	 * @param c Karte
	 */
	public void TakeCard(Card c)
	{
		hand_cards.add(c);
	}
	
	/**
	 * Gibt dem Spieler eine Karte (wenn er einen Stich gemacht hat)
	 * @param c Karte
	 */
	public void AddTrickCard(Card c)
	{
		trick_cards.add(c);
	}
	
	protected abstract Card GetPlayOutCard();
	
	/**
	 * Gibt die Karte zurück, die ausgespielt werden soll und löscht aus der Hand des Spielers.
	 * @return Karte oder null, wenn der Spieler noch nicht bereit ist (UI Eingabe erforderlich)
	 */
	public Card PlayOut()
	{
		Card c = GetPlayOutCard();
		hand_cards.remove(c);
		return c;
	}
	
	public void SetPoints(int points)	{ this.points = points; }
	public void AddPoint()				{ points++; }
	
	/**
	 * Akzeptiert den Trumpf oder lehnt ihn ab
	 * @param trump Trumpffarbe
	 * @return 1 wenn er akzeptiert wurde, -1 wenn nicht oder 0 wenn der Spieler noch nicht bereit ist
	 */
	public abstract int AcceptTrump(Card.Suit trump);
	
	/**
	 * Bestimmt den Trumpf gegen den Willen des Gegners oder nicht
	 * @param trump Trumpffarbe
	 * @return 1 wenn er bestimmt werden soll, -1 wenn nicht oder 0 wenn der Spieler noch nicht bereit ist
	 */
	public abstract int ForceTrump(Card.Suit trump);
	
	/**
	 * Entfernt unnötige Karten, wenn möglich
	 */
	public abstract void RemoveSpareCards();
	
	public abstract String GetName();
	public Card[] GetCards()						{ return hand_cards.toArray(new Card[hand_cards.size()]); }
	public Card[] GetTrickCards()					{ return trick_cards.toArray(new Card[trick_cards.size()]); }
	public int GetPoints()							{ return points; }
	public LinkedList<Card> GetCardsList()			{ return hand_cards; }
	public LinkedList<Card> GetTrickCardsList()		{ return trick_cards; }
}
